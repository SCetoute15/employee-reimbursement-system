# Employee Reimbursement System

# Project Description
Users are able to sign in and view all their prior reimbursements or create a new one.
Managers are able to sign in and approve or deny any pending reimbursements.

# Technologies Used
- JavaScript
- HTML
- CSS 
- AJAX
- SQL
- JDBC 
- Java 
- Selenium 
- Javalin 
- Mockito 
- JUnit 

# Features 
- Users can login with username and password 
- Users can create a new reimbursement request 
- Users can view all of their old requests 
- Manager can login with username and password 
- Manager can view all submitted requests 
- Manager can filter requests by status 
- Manager can approve or deny all pending reimbursements

# To-do 
- Ensure you can't submit a request after logging out and pressing the back button

# Getting Started 
- git clone https://gitlab.com/SCetoute15/employee-reimbursement-system.git












