/**
 * 
 */
window.onload = () => {    
    getSessionUser();
    getStatus(2);
    
}

function getSessionUser(){
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function(){
		
		if(xhttp.readyState == 4 && xhttp.status == 200 ){
			let user = JSON.parse(xhttp.responseText);
			
		}
	}
	
	xhttp.open("GET", "http://localhost:9003/user/session");
	
	xhttp.send();
}

function getStatus(status) {
    
    let xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = () => {
        if(xhttp.readyState == 4 && xhttp.status == 200) {
            let rList = JSON.parse(xhttp.responseText);
            loadData(rList);
        }
    }

    xhttp.open("GET", `http://localhost:9003/reimbs/status/${status}`);
    xhttp.send();
}

function loadData(rList) {

    let old = document.getElementsByTagName('tbody');
    let updated = document.createElement('tbody');
    old[0].parentNode.replaceChild(updated, old[0]);

    for (let reimb of rList) {

        let type = getType(reimb.type);
        let status = getStatusId(reimb.status);
        let data = document.createElement('tr');

        if(reimb.status == 2) {
            document.getElementById('approve').style.visibility = "visible";
            document.getElementById('deny').style.visibility = "visible";

            data.innerHTML += `<td>${reimb.author}</td>`;
            data.innerHTML += `<td>$${reimb.amount}</td>`;
            data.innerHTML += `<td>${reimb.submitted}</td>`;
 			data.innerHTML += `<td>${reimb.description}</td>`;
            data.innerHTML += `<td>${reimb.resolved}</td>`;
            data.innerHTML += `<td>${type}</td>`;
            data.innerHTML += `<td>${status}</td>`;
            data.innerHTML += `<td><button type="button" class="btn btn-success" onclick="approve(${reimb.id})">Approve</button></td>`;
            data.innerHTML += `<td><button type="button" class="btn btn-danger" onclick="deny(${reimb.id})">Deny</button></td>`;
        } else {
            document.getElementById('approve').style.visibility = "hidden";
            document.getElementById('deny').style.visibility = "hidden";

            data.innerHTML += `<td>${reimb.author}</td>`;
            data.innerHTML += `<td>$${reimb.amount}</td>`;
            data.innerHTML += `<td>${reimb.submitted}</td>`;
            data.innerHTML += `<td>${reimb.resolved}</td>`;
            data.innerHTML += `<td>${reimb.description}</td>`;
            data.innerHTML += `<td>${type}</td>`;
            data.innerHTML += `<td>${status}</td>`;
        }
        
        document.querySelector('#reimb-table > tbody').append(data);
    }
}

function getType(type) {

    switch(type) {
        case 1:
            return 'LODGING';
        case 2:
            return 'TRAVEL';
        case 3:
            return 'FOOD';
        case 4:
            return 'OTHER';
    }

}

function getStatusId(status) {

    switch(status) {
        case 1:
            return 'DENIED';
        case 2:
            return 'PENDING';
        case 3:
            return 'APPROVED';
    }

}

function approve(id) {
    
    let xhttp = new XMLHttpRequest();
    
    xhttp.onreadystatechange = () => {
        if(xhttp.readyState == 4) {
            if(xhttp.status == 404) {
                alert('Cant complete this request at this time :( ');
            } else if(xhttp.status == 200) {
                window.location.href = '/html/manager.html';
            }
        }
    }

    xhttp.open("POST", `http://localhost:9003/reimbs/${id}/approve`);
    xhttp.send();
}

function deny(id) {
    
    let xhttp = new XMLHttpRequest();
    
    xhttp.onreadystatechange = () => {
        if(xhttp.readyState == 4) {
            if(xhttp.status == 404) {
                alert('Cant complete this request at this time :( ');
            } else if(xhttp.status == 200) {
                window.location.href = `/html/manager.html`;
            }
        }
    }

    xhttp.open("POST", `http://localhost:9003/reimbs/${id}/deny`);
    xhttp.send();
}




