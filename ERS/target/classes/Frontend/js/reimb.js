/**
 * 
 */

window.onload = function(){
	console.log("JS is linked!");
	getReimbs();
}

function getReimbs(){
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function(){
		
		if(xhttp.readyState == 4 && xhttp.status == 200){
			let reimbs = JSON.parse(xhttp.responseText);
			
			loadData(reimbs);
		}
	}
	xhttp.open("GET", "http://localhost:9003/user/reimbs");
	
	xhttp.send();
}

function loadData(reimbs){
	for(let reimb of reimbs){
		let type = getType(reimb.type);
		let status = getStatus(reimb.status);
		let data = document.createElement("tr");
		
		data.innerHTML += `<td>${reimb.id}</td>`;
		data.innerHTML += `<td>${reimb.amount}</td>`;
		data.innerHTML += `<td>${reimb.description}</td>`;
		data.innerHTML += `<td>${type}</td>`;
		data.innerHTML += `<td>${reimb.submitted}</td>`;
		data.innerHTML += `<td>${reimb.resolved}</td>`;
		data.innerHTML += `<td>${status}</td>`;
		
		document.querySelector('#reimb-table > tbody').append(data);
	}
}

function getType(type) {

    switch(type) {
        case 1:
            return 'LODGING';
        case 2:
            return 'TRAVEL';
        case 3:
            return 'FOOD';
        case 4:
            return 'OTHER';
    }

}

function getStatus(status) {

    switch(status) {
        case 1:
            return 'DENIED';
        case 2:
            return 'PENDING';
        case 3:
            return 'APPROVED';
    }

}

