/**
 * Getting a Session User
 */

window.onload = function(){
	console.log("JS is linked!");
	getSessionUser();
	document.getElementById('submit').addEventListener('click', postReimbursement);
}

function getSessionUser(){
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function(){
		
		if(xhttp.readyState == 4 && xhttp.status == 200 ){
			let user = JSON.parse(xhttp.responseText);
			if(user.roleId == 2) {
                window.location.href = "/html/manager.html";
            }
		}
	}
	
	xhttp.open("GET", "http://localhost:9003/user/session");
	
	xhttp.send();
}

function getReimbs(){
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function(){
		
		if(xhttp.readyState == 4 && xhttp.status == 200){
			let reimbs = JSON.parse(xhttp.responseText);
			
			loadData(reimbs);
		}
	}
	xhttp.open("GET", "http://localhost:9003/user/reimbs");
	
	xhttp.send();
}

function loadData(reimbs){
	for(let reimb of reimbs){
		let type = getType(reimb.type);
		let status = getStatus(reimb.status);
		let reimb_data = document.createElement("tr");
		
		reimb_data.innerHTML += `<td>${reimb.id}</td>`;
		reimb_data.innerHTML += `<td>${reimb.amount}</td>`;
		reimb_data.innerHTML += `<td>${reimb.description}</td>`;
		reimb_data.innerHTML += `<td>${type}</td>`;
		reimb_data.innerHTML += `<td>${reimb.submitted}</td>`;
		reimb_data.innerHTML += `<td>${reimb.resolved}</td>`;
		reimb_data.innerHTML += `<td>${status}</td>`;
		
		document.querySelector('#reimb-table > tbody').append(reimb_data);
	}
}

function postReimbursement(){
	let newReimb = new FormData();
	
	newReimb.append("amount", document.getElementById('amount').value);
	newReimb.append("description", document.getElementById('description').value);
    newReimb.append("type", document.getElementById('type').value);
    
	
	let xhttp = new XMLHttpRequest();
	
	xhttp.onreadystatechange = function(){
		
		if(xhttp.readyState == 4 && xhttp.status == 200){
			window.location.href = "/html/home.html"
		}
	}
	
	xhttp.open("POST", "http://localhost:9003/reimbs/add");
	
	xhttp.send(newReimb);
}

function getType(type) {

    switch(type) {
        case 1:
            return 'LODGING';
        case 2:
            return 'TRAVEL';
        case 3:
            return 'FOOD';
        case 4:
            return 'OTHER';
    }

}

function getStatus(status) {

    switch(status) {
        case 1:
            return 'DENIED';
        case 2:
            return 'PENDING';
        case 3:
            return 'APPROVED';
    }

}

