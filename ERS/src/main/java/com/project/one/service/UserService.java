package com.project.one.service;

import com.project.one.dao.UserDao;
import com.project.one.model.User;

public class UserService {
	
	private UserDao uDao;
	
	public UserService() {
		// TODO Auto-generated constructor stub
	}
	
	public UserService(UserDao uDao) {
		super();
		this.uDao = uDao;
	}
	
	public User getUser(String username) {
		User u = uDao.findByUsername(username);
		if(u == null) {
			throw new NullPointerException();
		}
		return u;
	}
	
	public Boolean verifyLoginCredentials(String username, String password) {
		boolean isCorrect = false;
		User u = getUser(username);
		if(u.getPassword().equals(password)) {
			isCorrect = true;
		}
		return isCorrect;
	}
	

}
