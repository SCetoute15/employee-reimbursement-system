package com.project.one.service;

import java.util.List;

import com.project.one.dao.ReimbursementDao;
import com.project.one.model.Reimbursement;
import com.project.one.model.User;

public class ReimbursementService {
	
	private ReimbursementDao rDao;
	
	public ReimbursementService() {
		// TODO Auto-generated constructor stub
	}
	
	public ReimbursementService(ReimbursementDao rDao) {
		super();
		this.rDao = rDao;
	}
	
	public void addReimbursement(Reimbursement reimb ) {
		try {
		rDao.insertReimbursement(reimb);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Reimbursement> getByAuthor(User user){
		List<Reimbursement> rList = rDao.getByAuthor(user.getUserId());
		return rList;
	}
	
	public List<Reimbursement> getByStatus(int status){
		List<Reimbursement> rList = rDao.getByStatus(status);
		return rList;
	}
	
	public Reimbursement getById(int id){
		Reimbursement reimb = rDao.getById(id);
		return reimb;
	}
	
	public List<Reimbursement> getByResolver(int id){
		List<Reimbursement> rList = rDao.getByResolver(id);
		return rList;
	}
	
	public void updateReimbursement(Reimbursement newReimb) {
		try {
			rDao.update(newReimb);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	

}
