package com.project.one.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class HomePage {
	
	private WebDriver driver;

	private WebElement formHeader;
	private WebElement modal;
	private WebElement amount;
	private Select type;
	private WebElement description;
	private WebElement submitButton;

	public HomePage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.formHeader = driver.findElement(By.tagName("h5"));
		this.modal = driver.findElement(By.id("exampleModalLabel"));
		this.amount = driver.findElement(By.id("amount"));
		this.type = new Select(driver.findElement(By.id("type")));
		this.description = driver.findElement(By.id("description"));
		this.submitButton = driver.findElement(By.id("submit"));
	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:9003/html/home.html");
	}

	public void setAmount(int amount) {
		this.amount.clear();
		this.amount.sendKeys(String.valueOf(amount));
	}
	
	public String getAmount() {
		return this.amount.getAttribute("value");
	}
	
	public void setType(String select) {
		this.type.selectByVisibleText(select);
	}
	
	public void setDescription(String text) {
		this.description.clear();
		this.description.sendKeys(text);
	}
	
	public String getDescription() {
		return this.description.getAttribute("value");
	}
	
	public String getFormHeader() {
		return this.formHeader.getText();
	}
	
	public void submit() {
		this.submitButton.click();
	}
	
	public void click() {
		this.modal.click();
	}
	
}
