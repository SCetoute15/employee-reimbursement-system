package com.project.one.page;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ManagerPage {
	
	private WebDriver driver;
	
	private List<WebElement> approveButton;
	private List<WebElement> denyButton;
	
	public ManagerPage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.approveButton = driver.findElements(By.className("approve"));
		this.denyButton = driver.findElements(By.className("deny"));
	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:9003/html/manager.html");
	}
	
	public void approveFirst() {
		this.approveButton.get(0).click();
	}
	
	public void denyFirst() {
		this.denyButton.get(0).click();
	}



}
