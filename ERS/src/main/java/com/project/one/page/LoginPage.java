package com.project.one.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage {
	
	private WebDriver driver;	
	private WebElement header;
	private WebElement username;
	private WebElement password;
	private WebElement submitButton;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
		this.navigateTo();
		
		this.header = driver.findElement(By.tagName("h2"));
		this.username = driver.findElement(By.name("username"));
		this.password = driver.findElement(By.name("password"));
		this.submitButton = driver.findElement(By.id("submit"));
	}
	
	public void setUsername(String username) {
		this.username.clear();
		this.username.sendKeys(username);
	}
	
	public String getUsername() {
		return this.username.getAttribute("value");
	}
	
	public void setPassword(String password) {
		this.password.clear();
		this.password.sendKeys(password);
	}
	
	public String getPassword() {
		return this.password.getAttribute("value");
	}
	
	public String getHeader() {
		return this.header.getText();
	}
	
	public void submit() {
		this.submitButton.click();
	}
	
	public void navigateTo() {
		this.driver.get("http://localhost:9003/html/index.html");
	}
}