package com.project.one.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.project.one.model.User;
import com.project.one.model.UserRoles;

public class UserDao implements GenericUserDao<User>{
	
	private ERSDBConnection edc;
	
	public UserDao() {
		// TODO Auto-generated constructor stub
	}
	
	public UserDao(ERSDBConnection edc) {
		this.edc = edc;
	}

	@Override
	public User findByUsername(String username) {
		User u = null;
		
		try(Connection con = edc.getDBConnection()){
		
			String sql = "SELECT * FROM Users WHERE username = ?";
		
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, username);
			ResultSet rs = ps.executeQuery();
		
			if(!rs.first()) {
				return u;
			}
		
			u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return u;
	}
	
	@Override
	public User findById(int id) {
		User u = null;
		
		try(Connection con = edc.getDBConnection()){
			
			String sql = "SELECT * FROM Users WHERE users_id = ?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			if(!rs.first()) {
				return u;
			}
			
			u = new User(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getInt(7));			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return u;
	}
	
}
