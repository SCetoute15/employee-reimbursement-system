package com.project.one.dao;


import com.project.one.model.User;
import com.project.one.model.UserRoles;

import java.util.List;

public interface GenericUserDao <E> {
	
	public E findByUsername(String username);
	public E findById(int id);
}
