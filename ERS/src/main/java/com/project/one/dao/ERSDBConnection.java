package com.project.one.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ERSDBConnection {
	
	private String url = "jdbc:mariadb://database-1.cfvoxmpubq7s.us-east-2.rds.amazonaws.com:3306/ersdb";
	private String username = "ersuser";
	private String password = "mypassword";
	
	public Connection getDBConnection() throws SQLException{
		return DriverManager.getConnection(url, username, password);
	}

}
