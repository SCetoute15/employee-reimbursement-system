package com.project.one.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.project.one.model.Reimbursement;

public class ReimbursementDao implements GenericReimbursementDao<Reimbursement> {
	
private ERSDBConnection edc;
	
	public ReimbursementDao() {
		// TODO Auto-generated constructor stub
	}
	
	public ReimbursementDao(ERSDBConnection edc) {
		this.edc = edc;
	}
	
	@Override
	public void insertReimbursement(Reimbursement reimb) {
		try(Connection con = edc.getDBConnection()){
			String sql = "{ CALL create_reimb(?, ?, ?, 2, ?) }";
			
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, reimb.getAmount());
			cs.setString(2, reimb.getDescription());
			cs.setInt(3, reimb.getAuthor());
			cs.setInt(4, reimb.getType());
			cs.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public Reimbursement getById(int id) {
		Reimbursement reimb = null;
		
		try(Connection con = edc.getDBConnection()){
			String sql = "SELECT * FROM reimbursement WHERE reimb_id = ?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				reimb = new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return reimb;
	}

	@Override
	public List<Reimbursement> getByStatus(int status) {
		List<Reimbursement> rList = new ArrayList<>();
		
		try(Connection con = edc.getDBConnection()){
			String sql = "SELECT * FROM reimbursement WHERE status_id = ?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, status);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				rList.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rList;
	}

	@Override
	public List<Reimbursement> getByResolver(int id) {
		List<Reimbursement> rList = new ArrayList<>();
		
		try(Connection con = edc.getDBConnection()){
			String sql = "SELECT * FROM reimbursement WHERE resolver = ?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				rList.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rList;
	}
	
	@Override
	public List<Reimbursement> getByAuthor(int author){
		List<Reimbursement> rList = new ArrayList<>();
		
		try(Connection con = edc.getDBConnection()){
			String sql = "SELECT * FROM reimbursement WHERE author = ?";
			
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, author);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				rList.add(new Reimbursement(rs.getInt(1), rs.getInt(2), rs.getTimestamp(3), rs.getTimestamp(4), rs.getString(5), rs.getBlob(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return rList;
	}

	@Override
	public void update(Reimbursement entity) {
		try(Connection con = edc.getDBConnection()){
			String sql = "{ CALL update_reimb(?, ?, ?, ?) }";
			
			CallableStatement cs = con.prepareCall(sql);
			cs.setInt(1, entity.getStatus());
			cs.setInt(2, entity.getResolver());
			cs.setTimestamp(3, entity.getResolved());
			cs.setInt(4, entity.getId());
			cs.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
}
