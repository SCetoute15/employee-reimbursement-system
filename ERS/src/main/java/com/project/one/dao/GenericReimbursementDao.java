package com.project.one.dao;

import java.util.List;

import com.project.one.model.User;
import com.project.one.model.UserRoles;

public interface GenericReimbursementDao <E> {
	
	// ------------------- CRUD Methods -------------------------
		// Create(Insert) Method
		public void insertReimbursement(E reimb);
		
		// Read Methods
		public E getById(int id);
		public List<E> getByStatus(int status);
		public List<E> getByResolver(int id);
		public List<E> getByAuthor(int author);
		
		// Update Methods 
		public void update(E entity);

}
