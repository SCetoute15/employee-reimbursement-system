package com.project.one.model;

public class ReimbursementType {
	
	private int id;
	private String type;
	
	public ReimbursementType() {
		// TODO Auto-generated constructor stub
	}
	
	public ReimbursementType(int id, String type) {
		super();
		this.id = id;
		this.type = type;
	}

	public int getId() {
		return id;
	}

	public String getType() {
		return type;
	}
	
}
