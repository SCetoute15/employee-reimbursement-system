package com.project.one.model;

public class UserRoles {
	
	private int id;
	private String role;
	
	public UserRoles() {
		// TODO Auto-generated constructor stub
	}

	public UserRoles(int id, String role) {
		super();
		this.id = id;
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public String getRole() {
		return role;
	}
	
}
