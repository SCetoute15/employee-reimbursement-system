package com.project.one.controller;

import com.project.one.model.User;
import com.project.one.service.UserService;

import io.javalin.http.Handler;

public class UserController {
	
	private UserService uServ;
	
	public Handler userLogin = (ctx) ->{
		User user = (User) ctx.sessionAttribute("user");
		if(uServ.verifyLoginCredentials(ctx.formParam("username"), ctx.formParam("password"))) {
			System.out.println("Successful login!");
			ctx.sessionAttribute("user", uServ.getUser(ctx.formParam("username")));
			ctx.redirect("/html/home.html");
		} else {
			ctx.redirect("/html/failed.html");
		}
	};
	
	public Handler getSessUser = (ctx) ->{
		System.out.println((User)ctx.sessionAttribute("user"));
		User user = (User)ctx.sessionAttribute("user");
		ctx.json(user);
	};
	
	public UserController() {
		// TODO Auto-generated constructor stub
	}
	
	public UserController(UserService uServ) {
		super();
		this.uServ = uServ;
	}

}
