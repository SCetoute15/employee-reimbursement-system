package com.project.one.controller;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import com.project.one.model.Reimbursement;
import com.project.one.model.User;
import com.project.one.service.ReimbursementService;

import io.javalin.http.Handler;

public class ReimbursementController {

	private ReimbursementService rServ;
	
	public ReimbursementController() {
		// TODO Auto-generated constructor stub
	}

	public ReimbursementController(ReimbursementService rServ) {
		super();
		this.rServ = rServ;
	}

	public Handler addReimbursement = (ctx) ->{
			System.out.println("Reached the endpoint");
			int type = 0;
			try {
				type = Integer.parseInt(ctx.formParam("type"));
			} catch (NumberFormatException e) {
				System.out.println(" catch here");
			} 
//			if(type <= 0) {
//				System.out.println("if stmt");
//				return;
//			}
		int amount = Integer.parseInt(ctx.formParam("amount"));
		String description = ctx.formParam("description");
		int author =((User)ctx.sessionAttribute("user")).getUserId();
		Timestamp now = Timestamp.valueOf(LocalDateTime.now());
		System.out.println("before insertion");
		Reimbursement newReimb = new Reimbursement(amount, now, description, author, type);
		
		rServ.addReimbursement(newReimb);
	};
	
	public Handler approveReimbursement = (ctx) ->{
		User sessUser = (User)ctx.sessionAttribute("user");
		Reimbursement approve = rServ.getById(Integer.parseInt(ctx.pathParam("id")));
		if(approve.getAuthor() == sessUser.getUserId()) {
			return;
		}
		approve.setResolved(Timestamp.valueOf(LocalDateTime.now()));
		approve.setResolver(sessUser.getUserId());
		approve.setStatus(3);
		
		rServ.updateReimbursement(approve);
	};
	
	public Handler denyReimbursement = (ctx) ->{
		User sessUser = (User)ctx.sessionAttribute("user");
		Reimbursement deny = rServ.getById(Integer.parseInt(ctx.pathParam("id")));
		if(deny.getAuthor() == sessUser.getUserId()) {
			return;
		}
		deny.setResolved(Timestamp.valueOf(LocalDateTime.now()));
		deny.setResolver(sessUser.getUserId());
		deny.setStatus(1);
		
		rServ.updateReimbursement(deny);
	};
	
	public Handler getReimbursementByAuthor = (ctx) ->{
		User sessUser = (User)ctx.sessionAttribute("user");
		List<Reimbursement> rList = rServ.getByAuthor(sessUser);
		ctx.json(rList);	
	};
	
	public Handler getReimbursementByStatus = (ctx) ->{
		int status = Integer.parseInt(ctx.pathParam("status"));
		List<Reimbursement> rList = rServ.getByStatus(status);
		ctx.json(rList);	
	};
}
