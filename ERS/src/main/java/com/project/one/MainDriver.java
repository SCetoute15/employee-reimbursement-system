package com.project.one;

import com.project.one.controller.ReimbursementController;
import com.project.one.controller.UserController;
import com.project.one.dao.ERSDBConnection;
import com.project.one.dao.ReimbursementDao;
import com.project.one.dao.UserDao;
import com.project.one.service.ReimbursementService;
import com.project.one.service.UserService;

import io.javalin.Javalin;

public class MainDriver {

	public static void main(String[] args) {
		
		UserController uCon = new UserController(new UserService(new UserDao(new ERSDBConnection())));
		ReimbursementController rCon = new ReimbursementController(new ReimbursementService(new ReimbursementDao(new ERSDBConnection())));
		
		Javalin app = Javalin.create(config->{
			config.addStaticFiles("/Frontend");
		});
		
		app.start(9003);
		
		app.post("/user/login", uCon.userLogin);
		app.get("/user/session", uCon.getSessUser);
		
		app.get("/user/reimbs", rCon.getReimbursementByAuthor);
		app.get("/reimbs/status/:status", rCon.getReimbursementByStatus);
		app.post("/reimbs/add", rCon.addReimbursement);
		app.post("/reimbs/:id/approve", rCon.approveReimbursement);
		app.post("/reimbs/:id/deny", rCon.denyReimbursement);
		
		app.exception(NullPointerException.class, (e, ctx)->{
			ctx.status(404);
			ctx.result("ERROR!!! USER DOESN'T EXIST");
		});

	}

}
