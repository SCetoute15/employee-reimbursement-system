package com.project.one.service.eval;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project.one.dao.ReimbursementDao;
import com.project.one.model.Reimbursement;
import com.project.one.service.ReimbursementService;


public class ReimbursementServiceEval {
	
	@Mock 
	private ReimbursementDao rDao;
	
	private ReimbursementService testServ;
	private Reimbursement test;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		testServ = new ReimbursementService(rDao);
		Timestamp submitted = Timestamp.valueOf(LocalDateTime.of(2021, 01, 03, 14, 22, 07));
		Timestamp resolved = Timestamp.valueOf(LocalDateTime.of(2021, 01, 11, 13, 33, 57));
		test = new Reimbursement(50, 150, submitted, resolved, "Stuff", null, 3, 2, 1, 4);
		
		when(rDao.getById(70)).thenReturn(test);
	}
	
	@Test
	public void testSelectByIdSuccess() {
		assertEquals(testServ.getById(70), test);
	}

	@Test
	public void testSelectByIdFail() {
		assertNotEquals(testServ.getById(365), test);
	}



}
