package com.project.one.service.eval;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project.one.dao.UserDao;
import com.project.one.model.User;
import com.project.one.service.UserService;


public class UserServiceEval {
	
	@Mock
	private UserDao uDao;
	
	
	private UserService testServ;
	private User test;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		testServ = new UserService(uDao);
		test = new User(7, "schalumeau", "password", "Samantha", "Chalumeau", "test@gmail.com", 2);

		when(uDao.findById(7)).thenReturn(test);
		when(uDao.findByUsername("schalumeau")).thenReturn(test);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testGetUserSuccess() {
		assertEquals(testServ.getUser("schalumeau"), test);
	}

	@Test(expected = NullPointerException.class)
	public void testGetUserFail() {
		assertNotEquals(testServ.getUser("wrong person"), null);
	}

	
	@Test
	public void testVerifyLoginCredentialsSuccess() {
		User login = testServ.getUser("schalumeau");
		assertTrue(login.getPassword() == "password");
		
	}

	@Test
	public void testVerifyLoginFail() {
		User login = testServ.getUser("schalumeau");
		assertFalse(login.getPassword() == "notPassword");
	}
}

