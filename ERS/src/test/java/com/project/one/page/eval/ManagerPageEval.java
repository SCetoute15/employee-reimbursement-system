package com.project.one.page.eval;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.project.one.page.LoginPage;
import com.project.one.page.ManagerPage;



public class ManagerPageEval {
	
	private LoginPage page;
	private ManagerPage mgrPage;
	private static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {	
		File file =  new File("src/main/resources/chromedriver");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.page = new LoginPage(driver);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testManagerLogin() {
		page.setUsername("scetoute");
		page.setPassword("GOAT23");
		page.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/manager.html"));
		this.mgrPage = new ManagerPage(driver);
		
		assertEquals("http://localhost:9003/html/manager.html", driver.getCurrentUrl());
	}
	
	@Test
	public void testApproveReimb() throws InterruptedException {
		page.setUsername("scetoute");
		page.setPassword("GOAT23");
		page.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/manager.html"));
		this.mgrPage = new ManagerPage(driver);
		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.className("approve"))));
		
		int oldApproveList = driver.findElements(By.className("approve")).size();
		mgrPage.approveFirst();
		Thread.sleep(5000);
		int newApproveList = driver.findElements(By.className("approve")).size();
		
		assertEquals(oldApproveList - 1, newApproveList);
		
	}

	@Test
	public void testDenyReimb() throws InterruptedException {
		page.setUsername("scetoute");
		page.setPassword("GOAT23");
		page.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/manager.html"));
		this.mgrPage = new ManagerPage(driver);
		wait.until(ExpectedConditions.visibilityOfAllElements(driver.findElements(By.className("deny"))));
		
		int oldApproveList = driver.findElements(By.className("deny")).size();
		mgrPage.denyFirst();
		Thread.sleep(5000);
		int newApproveList = driver.findElements(By.className("deny")).size();
		
		assertEquals(oldApproveList - 1, newApproveList);
		
	}

}
