package com.project.one.page.eval;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.project.one.page.LoginPage;

public class LoginPageEval {
	
	private LoginPage page;
	private static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {	
		File file =  new File("src/main/resources/chromedriver");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.page = new LoginPage(driver);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testLoginHeader() {
		assertEquals(page.getHeader(), "Employee Reimbursement System");
	}
	
	@Test
	public void testSuccessfulLogin() {
		page.setUsername("scurry");
		page.setPassword("splashbro");
		page.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.urlMatches("/home.html"));
		
		assertEquals("http://localhost:9003/html/home.html", driver.getCurrentUrl());
	}
	
	@Test
	public void testSuccessfulManagerLogin() {
		page.setUsername("scetoute");
		page.setPassword("GOAT23");
		page.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/manager.html"));
		assertEquals("http://localhost:9003/html/manager.html", driver.getCurrentUrl());
		
	}
	
	@Test
	public void TestFailedLogin() {
		page.setUsername("dwade");
		page.setPassword("more junk");
		page.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/failed.html"));
		
		assertEquals("http://localhost:9003/html/failed.html", driver.getCurrentUrl());
	}

	
}