package com.project.one.page.eval;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.project.one.page.LoginPage;
import com.project.one.page.HomePage;


public class HomePageEval {
	
	private LoginPage loginPage;
	private HomePage homePage;
	private static WebDriver driver;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {	
		File file =  new File("src/main/resources/chromedriver");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
		
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		driver.quit();
	}
	
	@Before
	public void setUp() throws Exception {
		this.loginPage = new LoginPage(driver);
	}
	
	@After
	public void tearDown() throws Exception {
		
	}
	
	@Test
	public void testSuccessfulFormSubmit() {
		loginPage.setUsername("scurry");
		loginPage.setPassword("splashbro");
		loginPage.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/home.html"));
		
		this.homePage = new HomePage(driver);
		homePage.click();
		homePage.setAmount(789);
		homePage.setDescription("Testing 123");
		homePage.setType("Other");
		homePage.submit();
	}

	@Test
	public void testFailedFormSubmit() {
		loginPage.setUsername("dwade");
		loginPage.setPassword("retired3");
		loginPage.submit();
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.urlMatches("/home.html"));
		
		this.homePage = new HomePage(driver);
		homePage.click();
		homePage.submit();
	}


	
}
