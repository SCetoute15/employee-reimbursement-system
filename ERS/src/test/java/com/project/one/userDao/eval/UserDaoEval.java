package com.project.one.userDao.eval;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project.one.dao.ERSDBConnection;
import com.project.one.dao.UserDao;
import com.project.one.model.User;


public class UserDaoEval{
	@Mock
	private Connection con;
	
	@Mock 
	private ERSDBConnection edc;
	
	@Mock
	private ResultSet rs;
	
	@Mock
	private PreparedStatement ps;
	
	private User test;
	
	@BeforeClass
	public static void setUpBeforeClass()  throws Exception {
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		
	}
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(edc.getDBConnection()).thenReturn(con);
		when(con.prepareStatement(any(String.class))).thenReturn(ps);
		test = new User(7, "schalumeau", "password", "Samantha", "Chalumeau", "test@gmail.com", 2);
		when(rs.first()).thenReturn(true);
		when(rs.getInt(1)).thenReturn(test.getUserId());
		when(rs.getString(2)).thenReturn(test.getUsername());
		when(rs.getString(3)).thenReturn(test.getPassword());
		when(rs.getString(4)).thenReturn(test.getfName());
		when(rs.getString(5)).thenReturn(test.getlName());
		when(rs.getString(6)).thenReturn(test.getEmail());
		when(rs.getInt(7)).thenReturn(test.getRoleId());
		when(ps.executeQuery()).thenReturn(rs);
	}
	
	@After
	public void tearDown() throws Exception {
		
		
	}
	
	@Test
	public void testFindByIdSuccess() {
		assertEquals(new UserDao(edc).findById(7).getUserId(), test.getUserId());
	}
	
	@Test
	public void testFindByIdFailure() {
		assertNotEquals(new UserDao(edc).findById(12), test);
	}
	
	@Test
	public void testFindByUsernameSuccess() {
		assertEquals(new UserDao(edc).findByUsername("schalumeau").getUsername(), test.getUsername());
	}
	
	@Test
	public void testFindByUsernameFailure() {
		assertNotEquals(new UserDao(edc).findByUsername("zuko"), test);
	}

}